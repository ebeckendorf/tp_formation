﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    sealed class Ouvrier : Employe
    {
        public int dateEntree { get; set; }


        public const int SMIG=2500;
        

        public float salaire;

        
        public float Salaire
        {
           get {
                return salaire;
            }

            set
            {
                salaire = SMIG + (anneeEnCours - dateNaissance) * 100;

                if (salaire > SMIG*2)
                {
                    salaire = SMIG * 2;
                }
            }
        }

        private int anneeEnCours = DateTime.Now.Year;

        public Ouvrier(string matricule, string nom, string prenom, int dateNaissance, int dateEntree) : base(matricule, nom, prenom, dateNaissance)
        {
            this.dateEntree = dateEntree;
            this.dateNaissance = dateNaissance;
            Salaire = 0;
           
        }

        public override float GetSalaire()
        {
            return Salaire;
        }


    }
}
