﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    abstract class Employe
    {

        public string matricule { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public int dateNaissance { get; set; }

        public Employe(string matricule, string nom, string prenom, int dateNaissance)
        {
            this.matricule = matricule;
            this.nom = nom;
            this.prenom = prenom;
            this.dateNaissance = dateNaissance;
        }

        public void tostring()
        {
            Console.WriteLine(nom+ "  "+prenom+ ":  Salaire: "+ GetSalaire());
        }

        public abstract float GetSalaire();
        

    }
}
