﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    sealed class Cadre : Employe
    {
        public int indice { get; set; }

        public Cadre(string matricule, string nom, string prenom, int dateNaissance, int indice) : base(matricule, nom, prenom, dateNaissance)
        {
            this.indice = indice;
            Salaire = 0;

        }


        public float salaire;

        public float Salaire
        {
            get { return salaire; }
            set {
                    switch (indice)
                    {
                    case 1: salaire = 13000; break;
                    case 2: salaire = 15000; break;
                    case 3: salaire = 17000; break;
                    case 4: salaire = 20000; break; 
                            
                    }

                }
        }



        public override float GetSalaire()
        {
            return Salaire;
        }
    }
}
