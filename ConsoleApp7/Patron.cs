﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    sealed class Patron: Employe
    {
        static int CA = 500000;
        public int pourcentage { get; set; }

        private int salaire;

        public Patron(string matricule, string nom, string prenom, int dateNaissance, int pourcentage) : base(matricule, nom, prenom, dateNaissance)
        {

            
            this.pourcentage = pourcentage;
            Salaire = 0;
           
        }

        public int Salaire
        {
            get { return salaire; }
            set {
                    salaire = CA*pourcentage/ 100;
                }
        }





        public override float GetSalaire()
        {
            return Salaire;
        }
    }
}
